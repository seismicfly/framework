
app.controller('stockDetailsController', [
    '$scope', '$q', '$location', 'Toast', 'DataTable', 'INJECTIONS', 'Product', 'Stock',
    function ($scope, $q, $location, Toast, DataTable, INJECTIONS, Product, Stock) {
        var vm = this;
        vm.stock = {};
        vm.product = {};
        
        $q.all([
            Stock.get({id: INJECTIONS.id}).$promise.then(function (data) {
                return data.data;
            }, function (error) {
                Toast.error(error.data.msg);
            }),
            Product.get({id: INJECTIONS.query_params.pid}).$promise.then(function (data) {
                return data.data;
            }, function (error) {
                Toast.error(error.data.msg);
            })
        ]).then(function (data) {
            vm.stock = data[0];
            vm.product = data[1];
            
//            console.log(vm.product);
            
            
            vm.stock.product_id = vm.product.product_id;
        });

        vm.save = function () {
            clearErrorsOnForm("dataForm");
            if (vm.stock.stock_id) {
                Stock.update({id: vm.stock.stock_id}, vm.stock).$promise.then(function (data) {
                    vm.stock = data.data;
                    Toast.success("Data saved successfully!");
//                    location.href = base_url + "product/" + vm.stock.product_id;
                }, function (error) {
                    processError(Toast, error)
                });
            } else {
                Stock.save(vm.stock).$promise.then(function (data) {
                    vm.stock = data.data;
                    Toast.success("Data saved successfully!");
//                    location.href = base_url + "product/" + vm.stock.product_id;
                }, function (error) {
                    processError(Toast, error)
                });
            }
        };
    }
]);


app.controller('productController', [
    '$scope', '$q', 'Toast', '$uibModal', 'DataTable', 'Product', 'Category',
    function ($scope, $q, Toast, $uibModal, DataTable, Product, Category) {
        var vm = this;
        vm.data = [];
        vm.categories = [];
        vm.selected_category_id = null;
        vm.productListTable = {
            sort: {},
            search: {},
            pagination: {
                number: 25,
                start: 0
            }
        };

        $q.all([
            Category.get({}).$promise.then(function (data) {
                return data.data;
            }, function (error) {
                Toast.error(error.data.msg);
            }),
        ]).then(function (data) {
            vm.categories = data[0];
        });

        vm.fetchData = function fetchData(tableState) {
            vm.isTableLoading = true;
//            vm.dataTableSettings = getDataTableSettings(tableState);
            DataTable.retrieveRecords(Product, tableState, {deleted: 0}).then(function (result) {
                vm.data = updateDataTableMetaData(tableState, result);
                vm.isTableLoading = false;
            });
        };
        
        vm.deleteProduct = function(product){
            var modalInstance = $uibModal.open({
                templateUrl: 'delete-confirm.html',
                controller: 'DeleteConfirmController',
                controllerAs: 'vm',
                resolve: {
                    id: function () {
                        return product.product_id;
                    },
                    title: function () {
                        return "Product";
                    },
                    name: function () {
                        return product.name;
                    }
                }
            });

            modalInstance.result.then(function () {
//                alert("Delete");
                Product.delete({id: product.product_id}).$promise.then(function (data) {
                    vm.fetchData(vm.productListTable);
                    Toast.success("Data deleted successfully!");
                }, function (error) {
                    processError(Toast, error)
                });
            });
        };

    }
]);


app.controller('productDetailsController', [
    '$scope', '$q', '$location', 'Toast', 'DataTable', 'INJECTIONS', 'moment', '$uibModal', 'Product', 'Category', 'Stock',
    function ($scope, $q, $location, Toast, DataTable, INJECTIONS, moment, $uibModal, Product, Category, Stock) {
        var vm = this;
        vm.product = {};
        vm.categories = [];
        vm.stockList = [];
        vm.stockListTable = {
            sort: {},
            search: {},
            pagination: {
                number: 10,
                start: 0
            }
        };

        $q.all([
            Category.get({}).$promise.then(function (data) {
                return data.data;
            }, function (error) {
                Toast.error(error.data.msg);
            }),
            Product.get({id: INJECTIONS.id}).$promise.then(function (data) {
                return data.data;
            }, function (error) {
                Toast.error(error.data.msg);
            })
        ]).then(function (data) {
            vm.categories = data[0];
            vm.product = data[1];
        });



        vm.save = function () {
//            $location.url(base_url + "product/55").replace();
//            return false;
//            
            clearErrorsOnForm("dataForm");
            if (vm.product.product_id) {
                Product.update({id: vm.product.product_id}, vm.product).$promise.then(function (data) {
                    vm.product = data.data;
                    Toast.success("Data saved successfully!");
                }, function (error) {
                    processError(Toast, error)
                });
            } else {
                Product.save(vm.product).$promise.then(function (data) {
                    vm.product = data.data;
                    Toast.success("Data saved successfully!");
                    location.href = base_url + "stock/0?pid=" + vm.product.product_id;
                }, function (error) {
                    processError(Toast, error)
                });
            }

        };

        vm.fetchStockData = function fetchStockData(tableState) {
//            console.log(tableState);
            if (vm.product.product_id) {
                vm.isTableLoading = true;
                DataTable.retrieveRecords(Stock, tableState, {product_id: vm.product.product_id, deleted: 0}).then(function (result) {
                    vm.stockList = updateDataTableMetaData(tableState, result);
                    vm.isTableLoading = false;
                });
            }
        };

        vm.deleteStockItem = function (stock) {
            var modalInstance = $uibModal.open({
                templateUrl: 'delete-confirm.html',
                controller: 'DeleteConfirmController',
                controllerAs: 'vm',
                resolve: {
                    id: function () {
                        return stock.stock_id;
                    },
                    title: function () {
                        return "Stock Item";
                    },
                    name: function () {
                        return stock.name;
                    }
                }
            });

            modalInstance.result.then(function () {
//                alert("Delete");
                Stock.delete({id: stock.stock_id}).$promise.then(function (data) {
                    vm.fetchStockData(vm.stockListTable);
                    Product.get({id: INJECTIONS.id}).$promise.then(function (data) {
                        vm.product = data.data;
                    }, function (error) {
                        Toast.error(error.data.msg);
                    });
                    Toast.success("Data deleted successfully!");
                }, function (error) {
                    processError(Toast, error)
                });
            });
        };
    }
]);

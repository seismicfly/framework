
function errorScreen(errors) {
    angular.forEach(errors, function (value, key) {
        $("#" + key).addClass("invalid");
        $("<span class='error-msg text-danger'>" + value + "</span>").insertAfter("#" + key);
    });
}

function clearErrorsOnForm(formName) {
    $("form[name='" + formName + "']").find('span.error-msg').remove();
    $("form[name='" + formName + "']").find('.invalid').removeClass("invalid");

}

function processError(Toast, error){
    Toast.error(error.data.msg);
    errorScreen(error.data.error_fields);
}

function getDataTableSettings(tableState){
    var pagination = tableState.pagination;
    var start  = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
    var number = pagination.number || 10;  // Number of entries showed per page.
    return {start: start, number: number};
}

function updateDataTableMetaData(tableState, result){
    tableState.pagination.numberOfPages = result.pages; //set the number of pages so the pagination can update
    tableState.pagination.totalItemCount = result.item_count;
    return result.data;
}



(function () {
    var origOpen = XMLHttpRequest.prototype.open;
    var directXhrCount = 0;

    XMLHttpRequest.prototype.open = function () {
        directXhrCount++;
//        $('#loader').addClass('loading');
        $('.pace').removeClass('pace-inactive');
        $('.pace').addClass('pace-active');

        function laodingCompleted() {
            directXhrCount--;
            if (directXhrCount === 0) {
//                $('#loader').removeClass('loading');
                $('.pace').removeClass('pace-active');
                $('.pace').addClass('pace-inactive');
            }
        }

        this.addEventListener('load', function () {
            laodingCompleted();
        });
        this.addEventListener('error', function () {
            laodingCompleted();
        });
        this.addEventListener('abort', function () {
            laodingCompleted();
        });

        origOpen.apply(this, arguments);
    };
})();
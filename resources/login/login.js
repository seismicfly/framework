
app.controller('loginController', [
    '$scope', 'Authenticate', 'Toast',
    function ($scope, Authenticate, Toast) {
        var vm = this;
        vm.user = {name: '', password: ''};

        vm.doLogin = function () {
            clearErrorsOnForm("dataForm");
            Authenticate.doLogin(vm.user).$promise.then(function (data) {
                Toast.success("Redirecting", "Login Successfull!");
                location.reload();
            }, function (error) {
                Toast.error(error.data.msg);
                errorScreen(error.data.error_fields);
            });
        }
    }
]);

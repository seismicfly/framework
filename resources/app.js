
var app = angular.module('app', [
    'ngRoute',
    'ui.router',
    'ngResource',
    'ngAnimate',
    'toastr',
    'frapontillo.bootstrap-switch',
    'ui.bootstrap',
    'ui.select',
    'ngSanitize',
    'smart-table',
    'moment-picker',
    'angularMoment'
]);

// Services ----------------------------------------
app.factory('Authenticate', function ($resource) {
    return $resource(base_url + 'api/login/:action', {}, {
        doLogin: {
            method: 'POST',
            params: {action: 'doLogin'}
        },
        doLogout: {
            method: 'GET',
            params: {action: 'doLogout'}
        }
    }); // Note the full endpoint address
});

app.factory('Product', function ($resource) {
    return $resource(base_url + 'api/product/:id', {},
            {'update': {method: 'PUT'}},
            {cancellable: true}); // Note the full endpoint address
});

app.factory('Category', function ($resource) {
    return $resource(base_url + 'api/category/:id', {},
            {'update': {method: 'PUT'}},
            {cancellable: true}); // Note the full endpoint address
});

app.factory('Stock', function ($resource) {
    return $resource(base_url + 'api/stock/:id', {},
            {'update': {method: 'PUT'}},
            {cancellable: true}); // Note the full endpoint address
});

app.factory('Settings', function ($resource) {
    return $resource(base_url + 'api/settings/:id', {},
            {'update': {method: 'PUT'}},
            {cancellable: true}); // Note the full endpoint address
});

app.factory('Metadata', function ($resource) {
    return $resource(base_url + 'api/metadata/:action', {},
            {'update': {method: 'PUT'}},
            {cancellable: true}); // Note the full endpoint address
});

app.factory('DataTable', ['$q', '$filter', '$timeout', 'Toast', function ($q, $filter, $timeout, Toast) {
        function retrieveRecords(resource, params, filters) {
            var start = (angular.isDefined(params))? params.pagination.start: 0;
            var number = (angular.isDefined(params))? params.pagination.number: 10;
//            console.log("resource", resource, "start", start, "number", number, "params", params);
            var deferred = $q.defer();
            resource.get({start: start, number: number, params: params, filters: filters}).$promise.then(function (data) {
                deferred.resolve({
                    data: data.data,
                    pages: data.pages,
                    item_count: data.item_count
                });
            }, function (error) {
                Toast.error("Error loading data. Please try again");
            });
            return deferred.promise;
        }
        return {
            retrieveRecords: retrieveRecords
        };
    }]);


// Toast ------------------------------------------
app.factory('Toast', ['toastr', function (toastr) {
        return {
            success: function (text, title) {
                if (title == null)
                    title = "Success!";
                toastr.success(text, title);
            },
            error: function (text, title) {
                if (title == null)
                    title = "Error!";
                toastr.error(text, title);
            }
        };
    }]);

app.config(function (toastrConfig) {
    angular.extend(toastrConfig, {
        autoDismiss: false,
        containerId: 'toast-container',
        maxOpened: 0,
        newestOnTop: true,
        positionClass: 'toast-top-center',
        preventDuplicates: false,
        preventOpenDuplicates: false,
        target: 'body',
        closeButton: true,
        animate: true
    });
});


app.config(['momentPickerProvider', function (momentPickerProvider) {
        momentPickerProvider.options({
            /* Picker properties */
            locale: 'en',
//        format: 'L LTS',
            format: 'LL',
//        format: 'YYYY-MM-DD',
            minView: 'decade',
            maxView: 'month',
            startView: 'month',
            autoclose: true,
            today: false,
            keyboard: false,
            updateOn: 'blur',
            /* Extra: Views properties */
            leftArrow: '<i class="fa fa-arrow-left" aria-hidden="true"></i>',
            rightArrow: '<i class="fa fa-arrow-right" aria-hidden="true"></i>',
//        yearsFormat: 'YYYY',
//        monthsFormat: 'MMM',
//        daysFormat: 'D',
//        hoursFormat: 'HH:[00]',
//        minutesFormat: moment.localeData().longDateFormat('LT').replace(/[aA]/, ''),
//        secondsFormat: 'ss',
            minutesStep: 5,
            secondsStep: 1
        });
    }]);


// Common Popups
app.controller('DeleteConfirmController', [
    '$uibModalInstance', 'id', 'title', 'name',
    function ($uibModalInstance, id, title, name) {
        var vm = this;
        vm.id = id;
        vm.title = title;
        vm.name = name;

        vm.no = function () {
            $uibModalInstance.dismiss('cancel');
        };

        vm.yes = function () {
            $uibModalInstance.close('confirmed');
        };
    }
]);


//app.config(['$routeProvider', function ($routeProvider) {
////        alert(APPPATH + 'views/' + controller + '/' + view_name + '.php');
//alert(base_url + 'product/1');
////        alert(js_controller);
////        alert(base_url + 'product/0');
////        alert(APPPATH + 'views/' + controller + '/' + view_name + '.php');
//        alert("sa");
//        $routeProvider
//                .when(base_url + 'product/0', {
//                    templateUrl: APPPATH + 'views/' + controller + '/product_details.php',
//                    controller: js_controller,
//                    controllerAs: 'vm',
//                    resolve: {
//                        rProduct: ['Product', 'INJECTIONS', function (Product, INJECTIONS) {
//                                alert(2);
//                                return Product.get({id: INJECTIONS.id}).$promise;
//                            }]
//                    }
//                });
//    }
//]);

//app.config(function($stateProvider, $urlRouterProvider) {
//    $urlRouterProvider.otherwise('/home');
//    $stateProvider
//        // HOME STATES AND NESTED VIEWS ========================================
//        .state('home', {
//            url: '/home'
////            templateUrl: 'partial-home.html'
//        })
//        
//        // ABOUT PAGE AND MULTIPLE NAMED VIEWS =================================
//        .state('about', {
//            // we'll get to this in a bit       
//        });     
//});
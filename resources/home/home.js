
app.controller('homeController', [
    '$scope', 'Authenticate', 'Toast',
    function ($scope, Authenticate, Toast) {
        var vm = this;
        vm.doLogout = function () {
            Authenticate.doLogout({}).$promise.then(function(data){
                location.reload();
            }, function(error){
                Toast.error(error.data.error);
            });
        }
    }
]);

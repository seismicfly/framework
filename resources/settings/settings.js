
app.controller('settingsController', [
    '$scope', '$q', '$location', 'Toast', 'Settings', 'Metadata',
    function ($scope, $q, $location, Toast, Settings, Metadata) {
        var vm = this;
        vm.settings = {};
        vm.stockSelections = {};
        
        $q.all([
            Settings.get({id: 1}).$promise.then(function (data) {
                return data.data;
            }, function (error) {
                Toast.error(error.data.msg);
            }),
            Metadata.get({action: 'stockselection'}).$promise.then(function (data) {
                return data.data;
            }, function (error) {
                Toast.error(error.data.msg);
            })
        ]).then(function (data) {
            vm.settings = data[0];
            vm.stockSelections = data[1];
        });

//        vm.save = function () {
//            clearErrorsOnForm("dataForm");
//            if (vm.stock.stock_id) {
//                Stock.update({id: vm.stock.stock_id}, vm.stock).$promise.then(function (data) {
//                    vm.stock = data.data;
//                    Toast.success("Data saved successfully!");
////                    location.href = base_url + "product/" + vm.stock.product_id;
//                }, function (error) {
//                    processError(Toast, error)
//                });
//            } else {
//                Stock.save(vm.stock).$promise.then(function (data) {
//                    vm.stock = data.data;
//                    Toast.success("Data saved successfully!");
////                    location.href = base_url + "product/" + vm.stock.product_id;
//                }, function (error) {
//                    processError(Toast, error)
//                });
//            }
//        };
    }
]);

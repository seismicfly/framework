
app.controller('categoryDetailsController', [
    '$scope', '$q', '$location', 'Toast', 'INJECTIONS', 'Category',
    function ($scope, $q, $location, Toast, INJECTIONS, Category) {
        var vm = this;
        vm.category = {};

        $q.all([
            Category.get({id: INJECTIONS.id}).$promise.then(function (data) {
                return data.data;
            }, function (error) {
                Toast.error(error.data.msg);
            }),
        ]).then(function (data) {
            vm.category = data[0];
        });

        vm.save = function () {
            clearErrorsOnForm("dataForm");
            if (vm.category.category_id) {
                Category.update({id: vm.category.category_id}, vm.category).$promise.then(function (data) {
                    vm.category = data.data;
                    Toast.success("Data saved successfully!");
                }, function (error) {
                    processError(Toast, error)
                });
            } else {
                Category.save(vm.category).$promise.then(function (data) {
                    vm.category = data.data;
                    // todo: change URL
                    Toast.success("Data saved successfully!");
                }, function (error) {
                    processError(Toast, error)
                });
            }

        };
    }
]);

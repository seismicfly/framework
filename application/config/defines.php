<?php

// Global
define("SITE_TITLE", "Site Title");
define("BASE_PATH", "http://localhost:8080/sms/");

// Databse
define("DB_HOST", "localhost");
define("DB_USER", "root");
define("DB_PASS", "123456");
define("DB_NAME", "stock_manager");

// User Mgmt
define("USER_SESSION", "StockMgmtUser");
define("USER_SESSION_DURATION", (60 * 24 * 365)); // In minutes

// MetaData
define("LOGIN_CONTROLLER", "login");
define("HOME_CONTROLLER", "home");

// Debug
define("DEBUG_MODE", true);
define("PROFILING", false);

// Search prefixes
/*
 * i - equal search     - identical
 * s - starts with      - start
 * e - ends with        - end
 * l - full like        - like
 * f - greater than     - from
 * t - less than        - to
 */
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	https://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There are three reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router which controller/method to use if those
  | provided in the URL cannot be matched to a valid route.
  |
  |	$route['translate_uri_dashes'] = FALSE;
  |
  | This is not exactly a route, but allows you to automatically route
  | controller and method names that contain dashes. '-' isn't a valid
  | class or method name character, so it requires translation.
  | When you set this option to TRUE, it will replace ALL dashes in the
  | controller and method URI segments.
  |
  | Examples:	my-controller/index	-> my_controller/index
  |		my-controller/my-method	-> my_controller/my_method
 */
$route['default_controller'] = 'login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['api/([a-zA-Z]+)/([a-zA-Z]+)'] = function ($controller, $action) {
    return "api/{$controller}/{$action}";
};

// api/controller/id [GET] -> get action
$route['api/([a-zA-Z]+)/([0-9]+)']['get'] = function ($controller, $id) { // for api get with id
    return "api/{$controller}/get/{$id}";
};

// api/controller/id [PUT] -> update action
$route['api/([a-zA-Z]+)/([0-9]+)']['put'] = function ($controller, $id) {
    return "api/{$controller}/update/{$id}";
};

// api/controller/id [DELETE] -> delete action
$route['api/([a-zA-Z]+)/([0-9]+)']['delete'] = function ($controller, $id) {
    return "api/{$controller}/delete/{$id}";
};

// api/controller [POST] -> add action
$route['api/([a-zA-Z]+)']['post'] = function ($controller) {
    return "api/{$controller}/create";
};

// api/controller [GET] -> query action
$route['api/([a-zA-Z]+)']['get'] = function ($controller) {
    return "api/{$controller}/query";
};

$route['([a-zA-Z]+)/([0-9]+)'] = function ($controller, $id) {// for detail page
    return "{$controller}/details/{$id}";
};

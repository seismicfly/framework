/**
 * Author:  SeismicFly
 * Created: Jan 12, 2017
 */

--
-- Database: `stock_manager`
--
CREATE DATABASE `stock_manager`;

use `stock_manager`;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `last_mod_by` int(11) NOT NULL,
  `last_mod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`category_id`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `name`, `is_active`, `deleted`, `created_by`, `created_date`, `last_mod_by`, `last_mod_date`) VALUES
(1, 'General', 1, 0, 5, '2016-11-22 00:26:54', 5, '2016-11-21 18:56:54');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_key` varchar(50) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `category_id` int(11) NOT NULL,
  `alert_threshold` int(11) DEFAULT '20',
  `selling_price` float(10,2) NOT NULL,
  `total_stock_count` float NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_mod_by` int(11) NOT NULL,
  `last_mod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`product_id`),
  UNIQUE KEY `product_key` (`product_key`),
  KEY `name` (`name`,`is_active`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_stock`
--

CREATE TABLE IF NOT EXISTS `product_stock` (
  `stock_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `purchase_date` date DEFAULT NULL,
  `purchase_price` float NOT NULL,
  `purchase_quantity` float NOT NULL,
  `available_quantity` float DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_mod_by` int(11) NOT NULL,
  `last_mod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`stock_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_hash` varchar(40) NOT NULL,
  `name` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_mod_by` int(11) DEFAULT NULL,
  `last_mod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`),
  KEY `user_name` (`name`),
  KEY `user_hash` (`user_hash`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_hash`, `name`, `password`, `created_by`, `created_date`, `last_mod_by`, `last_mod_date`) VALUES
(1, '1j1h1j1k2j3h3j3j3mwjnawlkjhlskjfhlkajshl', 's', '1', NULL, NULL, NULL, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user_session`
--

CREATE TABLE IF NOT EXISTS `user_session` (
  `session_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_hash` varchar(40) NOT NULL,
  `remember_me` tinyint(1) NOT NULL DEFAULT '0',
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `remember_me` (`remember_me`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `settings` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_selection` int(11) DEFAULT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `settings` 
    ADD `created_by` INT( 11 ) NOT NULL,
    ADD `created_date` DATETIME NOT NULL,
    ADD `last_mod_by` INT( 11 ) NOT NULL,
    ADD `last_mod_date` TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ;

INSERT INTO `settings` (`setting_id`, `stock_selection`, `created_by`, `created_date`, `last_mod_by`, `last_mod_date`)
VALUES (NULL , '1', '0', '2017-05-01 00:00:00', '0', CURRENT_TIMESTAMP);

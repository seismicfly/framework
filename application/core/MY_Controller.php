<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class BaseController extends CI_Controller {

    public $controller = '';
    public $js_controller = '';
    public $view_name = '';
    public $page_title = '';
    public $sub_section = '';
    public $view_vars = [];
    public $base_vars = [];
    public $model = '';

    const DETAILS = 'details';

    function __construct() {
        parent::__construct();
        $this->getRequestParams();
        if ( $this->controller != 'rest' ){
            Authenticator::authCheck($this->controller);
            $this->setSubSection();
            $this->setBaseVar("base_url", base_url());
            $this->setBaseVar("controller", $this->controller);
            $this->setBaseVar("view_name", $this->view_name);
            $this->setBaseVar("js_controller", $this->js_controller);
            $this->setBaseVar("APPPATH", APPPATH);
        }
    }

    protected function loadView($view_params = array()) {
        $view_params += ['var' => (object) $this->view_vars];
        $this->load->view("common/header");
        $this->load->view("{$this->controller}/{$this->view_name}.php", $view_params);
        $this->load->view("common/footer");
        $this->output->enable_profiler(PROFILING);
    }

    public function setSubSection($param = null) {
        $this->view_name = ($param == null) ? $this->controller : $this->controller . "_" . $param;
        $this->js_controller = ($param == null) ? $this->controller . 'Controller' : $this->controller . ucfirst($param) . 'Controller';
    }

    public function setVar($key, $value) {
        $this->view_vars[$key] = $value;
    }

    public function setBaseVar($key, $value) {
        $this->base_vars[$key] = $value;
    }
    
    protected function getRequestParams() {
        $this->payload = json_decode(file_get_contents('php://input'));
        $this->payload_array = json_decode(file_get_contents('php://input'), true);
        $this->query_params = (object)$this->input->get(null, true);
    }

}

class RestController extends BaseController {
    public $model = null;
    public $controller = 'rest';

    function __construct() {
        parent::__construct();
        if ( !is_null($this->model) ) 
            $this->load->model($this->model);
    }

    

    public function ok($response = [], $success = '', $extra_fields = []) {
        $result['status'] = true;
        $result['msg'] = $success;
        $result['data'] = $response;
        if ( $extra_fields ){
            $result += $extra_fields;
        }
        return $this->respond($result, 200);
    }

    public function error($error = '', $error_fields = [], $response = []) {
        $result['status'] = false;
        $result['msg'] = $error;
        $result['error_fields'] = $error_fields;
        $result['data'] = $response;
        return $this->respond($result, 409);
    }

    public function notFound( $response = []) {
        $result['status'] = false;
        $result['msg'] = "Not Found";
        $result['data'] = $response;
        return $this->respond($result, 404);
    }

    private function respond($response, $headerStatus) {
        return $this->output
                        ->enable_profiler(PROFILING)
                        ->set_content_type('application/json')
                        ->set_status_header($headerStatus)
                        ->set_output(json_encode($response));
    }
    
    private function getQueryFilters($parameters, $filters) {
        $query_filters = ( isset($parameters->search->predicateObject)? (array)$parameters->search->predicateObject: [] );
        if ( !empty($filters) ){
            foreach( $filters as $field => $value ){
                $query_filters['i'.$field] = $value;
            }
        }
        return $query_filters;
    }
    
    public function query(){ // model default function (GET)
        $model = $this->model;
        $parameters = isset($this->query_params->params)? json_decode($this->query_params->params): null;
        $filters = isset($this->query_params->filters)? json_decode($this->query_params->filters): null;
        $query_filters = $this->getQueryFilters($parameters, $filters);
        
//        echo "<pre>".print_r($query_filters, true)."</pre>";
        
        $order_asc = true;
        $order_by_col = '';
        
        if ( isset($parameters->sort->predicate) ){
            $order_by_col = $parameters->sort->predicate;
            $order_asc    = !$parameters->sort->reverse;
        }
        
        $result = $this->$model->query(
                $query_filters, // fltr
                isset($this->query_params->start)? $this->query_params->start: 0, // limit start
                isset($this->query_params->number)? $this->query_params->number: null, // limit
                $order_by_col, // order by col
                $order_asc // is order by acs
                );
        
        $response = $result['result'];
        unset($result['result']);
        return $this->ok($response, '', $result);
//        
//        echo "<pre>".print_r($model, true)."</pre>";
//        echo "<pre>".print_r($this->query_params, true)."</pre>";
//        echo "<pre>".print_r($parameters, true)."</pre>";
    }

    public function get($id) {
        $model = $this->model;
        $result = $this->$model->get($id);
        return $result? $this->ok($result): $this->notFound();
    }

    public function update($id) {
        $model = $this->model;
        $result = $this->$model->save($this->payload_array);
        return $result? $this->ok($result): $this->error('', $this->$model->getErrors());
//        return $result? $this->ok($result): $this->notFound();
        
//        echo "<pre>".print_r($result, true)."</pre>";
//        echo "<pre>".print_r($this->payload, true)."</pre>";
    }

    public function create() {
        $model = $this->model;
        $result = $this->$model->save($this->payload_array);
//        echo "<pre>".print_r($result, true)."</pre>";
//        echo "<pre>".print_r($this->payload, true)."</pre>";
//        exit;
        return $result? $this->ok($result): $this->error('', $this->$model->getErrors());
//        return $result? $this->ok($result): $this->notFound();
        
    }
    
    public function delete($id) {
        $model = $this->model;
        $result = $this->$model->get($id);
        if ( $result ){
            $save_delete = $this->$model->delete($id);
            return $save_delete? $this->ok($save_delete): $this->error('', $this->$model->getErrors());
        } else {
            return $this->notFound();
        }
    }
    
    // Validation related
//    public function is_unique_update($unique_check_col_value, $extra_params) {
//        list($unique_col_name, $modal_id_col, $modal_id) = explode(",", $extra_params);
////        var_dump($unique_check_col_value);
////        var_dump($unique_col_name);
////        var_dump($modal_id);
//        
//        $filters[$modal_id_col . " !="] = $modal_id;
//        $filters[$unique_col_name] = $unique_check_col_value;
//        
////        echo "<pre>*** filters ***".print_r($filters, true)."</pre>";
//        $model = $this->model;
//        $result = $this->$model->find($filters);
////        echo "<pre>".print_r($result, true)."</pre>";
//        return ( count($result) > 0 )? FALSE: TRUE;
//    }
}

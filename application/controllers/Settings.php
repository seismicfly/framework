<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends BaseController {

    public $controller = 'settings';
    public $page_title = 'Settings';

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->loadView();
    }

}

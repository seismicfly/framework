<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends BaseController {

    public $controller = 'product';
    public $page_title = 'Product';

    public function index() {
        $this->loadView();
    }
    
    public function details($id) {
        $this->setSubSection(self::DETAILS);
        
        $this->setVar('id', $id);
        $this->loadView();
    }

} 
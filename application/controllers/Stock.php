<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Stock extends BaseController {

    public $controller = 'stock';
    public $page_title = 'Stock';

    public function index() {
        $this->loadView();
    }
    
    public function details($id) {
        $this->setSubSection(self::DETAILS);
        
        $this->setVar('id', $id);
        $this->setVar('query_params', $this->query_params);
        $this->loadView();
    }

} 
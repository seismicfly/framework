<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends RestController {

    function __construct() {
        parent::__construct();
        $this->load->model('User');
        $this->load->model('UserSession');
//        exit();
    }

    public function doLogin() {
//        sleep(2);
        $fltr['name'] = $this->payload->name;
        $fltr['password'] = $this->payload->password;
        $user = $this->User->findFirst($fltr);
        
        if ($user) {
            if ($this->UserSession->deleteUnique($user->user_hash)) {
                $param['user_hash'] = $user->user_hash;
                $param['start_time'] = $this->UserSession->dateTime();
                $param['end_time'] = $this->UserSession->dateTime("+" . USER_SESSION_DURATION . " minutes");
                
                if ($this->UserSession->save($param)){
                    Authenticator::setAuthCookie($user->user_hash);
                    Authenticator::setUser($user);
                    return $this->ok();
                } else {
                    return $this->error('Unable to login. Please try again');
                }
            } else {
                return $this->error('Unable to login. Please try again');
            }
        } else {
            $test['name'] = 'Invalid Name';
            $test['password'] = 'Pw case';
            return $this->error('Invalid User', $test);
        }
    }

    public function doLogout() {
        $this->UserSession->deleteUnique(Authenticator::getAuthCookie());
        Authenticator::deleteAuthCookie();
        return $this->ok();
    }

}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Metadata extends RestController {

//    public $controller = 'metadata';
//    public $model = 'SettingsModel';
    
    public function stockselection() {
        $this->load->model('SettingsModel');
        $response = SettingsModel::getStockSelectionOptions();
        return ($response)? $this->ok($response): $this->error();
    }
    
}

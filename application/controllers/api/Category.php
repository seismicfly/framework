<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends RestController {

    public $controller = 'category';
    public $model = 'CategoryModel';
    
}

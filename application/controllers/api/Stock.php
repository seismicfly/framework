<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Stock extends RestController {

    public $controller = 'stock';
    public $model = 'StockModel';
    
}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends RestController {

    public $controller = 'settings';
    public $model = 'SettingsModel';
    
}

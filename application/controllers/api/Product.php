<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends RestController {

    public $controller = 'product';
    public $model = 'ProductModel';
    
}

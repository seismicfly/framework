<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends BaseController {

    public $controller = 'category';
    public $page_title = 'Category';

    public function index() {
        $this->loadView();
    }
    
    public function details($id) {
        $this->setSubSection(self::DETAILS);
        
        $this->setVar('id', $id);
        $this->loadView();
    }

} 
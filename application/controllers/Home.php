<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends BaseController {

    public $controller = 'home';
    public $page_title = 'Home';

    public function index() {
        $this->loadView();
    }

}

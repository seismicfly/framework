<?php

class SettingsModel extends BaseModel {

    const TABLE = 'settings';
    const ID_COL = 'setting_id';

    protected $setting_id = null;
    protected $stock_selection = null;
    protected $is_audit = true;
    protected $data_types = [
        'settings_id' => 'int',
        'stock_selection' => 'int'
    ];
    
    const STOCK_SELECTION_DEFAULT = 1;
    const STOCK_SELECTION_FIFO = 1;
    const STOCK_SELECTION_LIFO = 2;
    const STOCK_SELECTION_USER = 3;
    
    private static $stock_selection_options = [
        self::STOCK_SELECTION_FIFO => 'FIFO',
        self::STOCK_SELECTION_LIFO => 'LIFO',
        self::STOCK_SELECTION_USER => 'Select on POS',
    ];

    public function __construct() {
        parent::__construct();
    }

    protected function validate($params) {
        $this->setRule("settings_id", "required");
        return parent::validate($params);
    }
    
    public static function getStockSelectionOptions(){
        return self::$stock_selection_options;
    }

}

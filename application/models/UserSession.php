<?php

class UserSession extends BaseModel {

    const TABLE = 'user_session';
    const ID_COL = 'session_id';
    const UNIQUE_COL = 'user_hash';
    
    protected $is_audit = false;
    
    public $session_id = null;
    public $user_hash = null;
    public $remember_me = false;
    public $start_time = null;
    public $end_time = null;
    
    protected $data_types = [
        'session_id' => 'int',
        'remember_me' => 'bool',
    ];

    public function __construct() {
        parent::__construct();
//        $this->is_audit = false;
    }

}

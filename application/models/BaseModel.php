<?php

class BaseModel extends CI_Model {

    const TABLE = '';
    const ID_COL = '';
    const UNIQUE_COL = '';

    protected $is_audit = true;
    protected $soft_delete = true;
    protected $data_types = [];
    private $ignore_fields = ['is_audit', 'ignore_fields', 'data_types', 'soft_delete', 
        'audit_col_list', 'model_errors', 'validation_rules', 'relationships', 'temp_cache', 'CI'];
    private $audit_col_list = [
        'created_by' => 'int',
        'created_date' => 'datetime',
        'last_mod_by' => 'int',
        'last_mod_date' => 'datetime'];
    protected $model_errors = [];
    protected $validation_rules = [];
    protected $relationships = [];
    protected $temp_cache = [];
    protected $CI = null;

    public function __construct() {
        parent::__construct();
        $this->CI = get_instance();
    }

    public function setRelation($relationship_name, $field, $relative_model_name, $relative_col, $filter_cols = []) {
        $this->relationships[$field] = (object) [
                    'name' => $relationship_name,
                    'relative_model' => $relative_model_name,
                    'relative_column' => $relative_col,
                    'column_list' => $filter_cols
        ];
    }

    public function get($id) {
        $result = [];
        $query = $this->db->where(static::ID_COL, $id)
                ->get(static::TABLE);

        if ($id == 0) {
            $result = $this->postProcessDataRow([]);
        } elseif ($query->num_rows() == 1) {
            $result = $this->postProcessDataRow($query->row(0));
        }
        return $result;
    }

    public function getUnique($uniqueValue) {
        $query = $this->db->where(static::UNIQUE_COL, $uniqueValue)
                ->get(static::TABLE);

        if ($query->num_rows() == 1) {
            return $this->postProcessDataRow($query->row(0));
        }
        return [];
    }

    private function setFilters($fltr = []) {
        if ($fltr) {
            foreach ($fltr as $prefixed_field => $value) {
                $field = substr($prefixed_field, 1);
                $search_prefix = substr($prefixed_field, 0, 1);
                switch ($search_prefix) {
                    case 'i': // identical
                        $this->db->where($field, $value);
                        break;
                    case 'f': // from
                        $this->db->where("{$field} >=", $value);
                        break;
                    case 't': // to
                        $this->db->where("{$field} <=", $value);
                        break;
                    case 's': // starts with
                        $this->db->like($field, $value, 'after');
                        break;
                    case 'e': // ends with
                        $this->db->like($field, $value, 'before');
                        break;
                    default: // full like
                        $this->db->like($field, $value, 'both');
                        break;
                }
            }
        }
    }

    // query only allows columns names with search prefix defined in defines.php
    // this is used to query paged results
    public function query($fltr = [], $start = 0, $limit = null, $order_by_col = '', $order_asc = true) {
//        sleep(7);
//        if ( $fltr )
//            echo "<pre>*** fltr ***".print_r($fltr, true)."</pre>";
        $num_of_pages = 1;
        $this->setFilters($fltr);
        $num_of_records = $this->db->count_all_results(static::TABLE); // get result count before limiting

        $this->setFilters($fltr); // after CI runs query filters get null. have to set them again
        if ($order_by_col) {
            $this->db->order_by($order_by_col, ($order_asc ? 'ASC' : 'DESC'));
        }
        if ($limit) {
            $this->db->limit($limit, $start);
            if ($num_of_records > 0) {
                $num_of_pages = ceil((int) $num_of_records / (int) $limit);
            }
        }
        $query = $this->db->get(static::TABLE);
        $result = [];

//        echo "<pre>*** result ***".print_r($query->result(), true)."</pre>";

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data_row) {
                $result[] = $this->postProcessDataRow($data_row);
            }
        }
//        echo "<pre>*** for ***".print_r($result, true)."</pre>";

        $result['result'] = $result;
//        $result['result'] = ( $query->num_rows() > 0 )? $query->result(): [];
        $result['item_count'] = $num_of_records;
        $result['pages'] = $num_of_pages;
        return $result;
    }

    private function postProcessDataRow($data_row) {
        $type_casted_data = $this->castDataTypes($data_row);
        if ($this->relationships) {
            foreach ($this->relationships as $field => $relation_data) {
                if (isset($type_casted_data->$field)) {
                    $model = $relation_data->relative_model;
                    $append_key = $relation_data->name;
                    $CI = get_instance();
                    $CI->load->model($model);
                    
                    if ( isset($this->temp_cache[$model][$type_casted_data->$field]) ){
                        $related_record_raw = $related_record = $this->temp_cache[$model][$type_casted_data->$field];
                    } else {
                        $related_record_raw = $related_record = $CI->$model->findFirst([$relation_data->relative_column => $type_casted_data->$field]);
                        $this->temp_cache[$model][$type_casted_data->$field] = $related_record_raw;
                    }
                    
                    if ($relation_data->column_list) {
                        $related_record = (object) array_intersect_key((array) $related_record, array_flip($relation_data->column_list));
                    }
                    $type_casted_data->$append_key = $related_record;
                }
            }
        }
        return $type_casted_data;
    }

    public function find($fltr = [], $limit = false) {
        if ($fltr) {
            foreach ($fltr as $field => $value) {
                $this->db->where($field, $value);
            }
        }
        $query = $this->db->get(static::TABLE, $limit);
        if ($query->num_rows() > 0) {
            return $query->result(); // Cast Data
        }
        return [];
    }

    public function findFirst($fltr = []) {
        $response = $this->find($fltr, 1);
        if (isset($response[0])) {
            return $this->postProcessDataRow($response[0]);
        }
        return [];
    }

    public function delete($id) {
        $record = $this->get($id);
        $record = $this->beforeDelete($record);
        if ( $this->soft_delete ){
            $record->deleted = 1;
            $delete = $this->save($record);
        } else {
            $delete = $this->db->delete(static::TABLE, array(static::ID_COL => $id));
        }
        $record = $this->afterDelete($record);
        return $delete;
    }
    
    protected function beforeDelete($params) {
        return $params;
    }

    protected function afterDelete($params) {
        return $params;
    }

    public function deleteUnique($uniqueValue) {
//        echo static::TABLE."--------";
//        echo static::UNIQUE_COL."--------";
//        echo $uniqueValue."--------";

        return $this->db->delete(static::TABLE, array(static::UNIQUE_COL => $uniqueValue));
    }

    private function getIdColValue($params) {
        return isset($params[static::ID_COL]) ? $params[static::ID_COL] : false;
    }
    
    // Removes relationship specific columns from dataset
    private function filterParams($params) {
        $filtered_params = $params;
//        echo "<pre>*** params".print_r($params, true)."</pre>"; //exit();
//        echo "<pre>*** params".print_r($this->relationships, true)."</pre>"; //exit();
        if ($this->relationships) {
            foreach ($this->relationships as $field => $relation_data) {
                unset($filtered_params[$relation_data->name]);
            }
        }
//        echo "<pre>*** fltr params".print_r($filtered_params, true)."</pre>"; //exit();
        return $filtered_params;
    }

    public function save($params) {
//        echo "<pre>params".print_r($params, true)."</pre>";
        $params = is_object($params) ? (array) $params : $params;
//        echo "<pre>params (arr)".print_r($params, true)."</pre>";
        $params = $this->filterParams($params); // Removes relationship specific columns from dataset
//        echo "<pre>filter params".print_r($params, true)."</pre>";
        $params = $this->castDataTypes($params, 'array');
//        echo "<pre>casted params".print_r($params, true)."</pre>";
        $id_col_value = $this->getIdColValue($params);

        $params = $this->beforeSave($params);
//        echo "<pre>before save params".print_r($params, true)."</pre>";
        if (!$this->validate($params)) {
            return FALSE;
        }
//        exit;
        if ($id_col_value) {
            // update
            if (isset($params[$id_col_value])) {
                unset($params[$id_col_value]);
            }
            $params = $this->appendAuditCols($params);
//            echo "<pre>audit col appended params".print_r($params, true)."</pre>";
            $params = $this->beforeUpdate($params);
//            echo "<pre>before update params".print_r($params, true)."</pre>";
            
            if ( !$this->validateBeforeUpdate($params) ){
                return FALSE;
            }
//            exit; 
            
            $this->db->where(static::ID_COL, $id_col_value);
//            echo "<pre>+++++params".print_r($params, true)."</pre>";
            $result = $this->db->update(static::TABLE, $params);
            if ($result) {
//                echo 'updated';
                $last_updated_record = $this->get($id_col_value);
                $last_updated_record = $this->afterUpdate($last_updated_record);
                $last_updated_record = $this->afterSave($last_updated_record);
                return $last_updated_record;
            }
//            else {
//                return $test = $this->db->_error_message();
//                echo "<pre>+++ Error".print_r($test, true)."</pre>"; exit(0);
//            }
            return FALSE;
        } else {
            // insert
            $params = $this->appendAuditCols($params);
//            echo "<pre>audit col appended params".print_r($params, true)."</pre>";
            $params = $this->beforeInsert($params);
//            echo "<pre>before insert params".print_r($params, true)."</pre>";
            
            if ( !$this->validateBeforeInsert($params) ){
//                echo 'validation errors';
                return FALSE;
            }
//            exit;
            $result = $this->db->insert(static::TABLE, $params);
            if ($result) {
                $last_inserted_record = $this->get($this->db->insert_id());
                $last_inserted_record = $this->afterInsert($last_inserted_record);
                $last_inserted_record = $this->afterSave($last_inserted_record);
                return $last_inserted_record;
            }
            return FALSE;
        }
    }

    // DB Support
    private function appendAuditCols($params) {
        if ($this->is_audit) {
//            Authenticator::authCheck();
            $id_col_value = $this->getIdColValue($params);
            if (!$id_col_value) {
                $params['created_by'] = isset($params['created_by']) && $params['created_by'] ? $params['created_by'] : Authenticator::getUserId();
                $params['created_date'] = isset($params['created_date']) && $params['created_date'] ? $params['created_date'] : $this->dateTime();
            }
            $params['last_mod_by'] = Authenticator::getUserId();
            $params['last_mod_date'] = $this->dateTime();
        }
        return $params;
    }

    public function dateTime($timeChanger = null) {
        if ($timeChanger == null) {
            return date("Y-m-d H:i:s");
        } else {
            return date("Y-m-d H:i:s", strtotime($timeChanger));
        }
    }

    private function castDataTypes($data_row, $returnAs = 'object') {
        $data_row = is_array($data_row)? (object)$data_row: $data_row;
        $formatted_row = [];
//        echo "<pre>*** data_row ***".print_r($data_row, true)."</pre>";
        $model_vars = $this->getModelFields();
//        echo "<pre>*** modal_vars ***".print_r($model_vars, true)."</pre>";
        
        foreach ($model_vars as $field => $meta_data) {
            if (!empty((array)$data_row)) {
                $field_value = property_exists($data_row, $field) ? $data_row->$field : $this->$field;
            } else {
                // this block is to handle special data cast when id is 0
                $field_value = isset($data_row->$field) ? $data_row->$field : (!$this->isAuditCol($field) ? $this->$field : '' );
            }
            $field_data_type = $this->getFieldDataType($field);
            switch ($field_data_type) {
                case "int":
                    $value = ($field_value === null) ? null : (int) $field_value;
                    break;
                case "bool":
                    $value = (bool) $field_value;
                    break;
                case "float":
                    $value = (float) $field_value;
                    break;
                case "date":
                    $value = (string) ($field_value === null) ? "" : date("Y-m-d", strtotime($field_value));
                    break;
                default:
                    $value = (string) $field_value;
                    break;
            }
            $formatted_row[$field] = $value;
        }
        return ( 'object' == $returnAs )? (object) $formatted_row: (array) $formatted_row;
    }

    private function getFieldDataType($field) {
        if (isset($this->data_types[$field])) {
            return $this->data_types[$field];
        } elseif ($this->isAuditCol($field)) {
            return $this->audit_col_list[$field];
        }
        return 'string';
    }

    private function getModelFields() {
        $model_col_list = array_diff_key(get_object_vars($this), array_flip($this->ignore_fields));
//        var_dump($model_col_list);
        if ($this->is_audit) {
            foreach ($this->audit_col_list as $col => $dataType) {
                $model_col_list[$col] = '';
            }
        }

//        echo "<pre>".print_r($model_col_list, true)."</pre>";
        return $model_col_list;
    }

    private function isAuditCol($field) {
        return in_array($field, array_keys($this->audit_col_list));
    }

    protected function addModelError($field, $error) {
        if (!isset($this->model_errors[$field])) {
            $this->model_errors[$field] = $error;
        }
    }

    protected function setRule($field, $rules, $label = '') {
        $this->validation_rules[$field]['label'] = $label;
        $this->validation_rules[$field]['rules'] = $rules;
//        echo "<pre>".print_r($this->validation_rules, true)."</pre>"; exit();
    }

    private function setValidationRules() {
        foreach ($this->validation_rules as $field => $validation) {
            $this->form_validation->set_rules($field, $validation['label'], "trim|{$validation['rules']}");
        }
    }

    public function getErrors() {
        return $this->model_errors;
    }

    protected function beforeSave($params) {
        return $params;
    }

    protected function beforeInsert($params) {
        return $params;
    }

    protected function beforeUpdate($params) {
        return $params;
    }

    protected function afterSave($params) {
        return $params;
    }

    protected function afterInsert($params) {
        return $params;
    }

    protected function afterUpdate($params) {
        return $params;
    }

    protected function validate_common($params) {
        $this->form_validation->set_data($params);
        $this->setValidationRules();
        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            if ($errors) {
                $this->model_errors = $errors;
                return FALSE;
            }
        }
        return TRUE;
    }

    protected function validate($params) {
        return $this->validate_common($params);
    }

    protected function validateBeforeInsert($params) {
        return $this->validate_common($params);
    }

    protected function validateBeforeUpdate($params) {
        return $this->validate_common($params);
    }
    
    

}

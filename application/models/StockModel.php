<?php

class StockModel extends BaseModel {

    const TABLE = 'product_stock';
    const ID_COL = 'stock_id';

    protected $stock_id = null;
    protected $product_id = null;
    protected $name = null;
    protected $purchase_date = null;
    protected $purchase_price = null;
    protected $purchase_quantity = null;
    protected $available_quantity = null;
    protected $deleted = false;
    
    protected $is_audit = true;
    protected $data_types = [
        'stock_id' => 'int',
        'product_id' => 'int',
        'purchase_price' => 'float',
        'purchase_quantity' => 'float',
        'available_quantity' => 'float',
        'purchase_date' => 'date',
        'deleted' => 'bool'
    ];

    public function __construct() {
        parent::__construct();
    }

    protected function validate($params) {
        $this->setRule("name", "required");
        $this->setRule("purchase_date", "required");
        $this->setRule("purchase_price", "required");
        $this->setRule("purchase_quantity", "required");
        return parent::validate($params);
    }
    
    protected function beforeInsert($params) {
        $params['available_quantity'] = $params['purchase_quantity'];
        return $params;
    }
    
    protected function afterInsert($params) {
//        echo "<pre>*** afterInsert ***".print_r($params, true)."</pre>";
        $this->CI->load->model('ProductModel');
        $product = $this->CI->ProductModel->get($params->product_id);
//        echo "<pre>*** Product ***".print_r($product, true)."</pre>";
        $product->total_stock_count += (float)$params->available_quantity;
        if ($this->CI->ProductModel->save($product)){
            return $params;
        } 
//        else {
//            $this->db->_error_message();
//            $this->addModelError($field, $error);
//        }
        return false;
    }
    
    protected function afterDelete($params){
        $this->CI->load->model('ProductModel');
        $product = $this->CI->ProductModel->get($params->product_id);
        $product->total_stock_count -= (float)$params->available_quantity;
        $this->CI->ProductModel->save($product);
        return $params;
    }

}

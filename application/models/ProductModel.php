<?php

class ProductModel extends BaseModel {

    const TABLE = 'product';
    const ID_COL = 'product_id';
    const UNIQUE_COL = 'product_key';

    protected $product_id = null;
    protected $product_key = null;
    protected $name = null;
    protected $description = null;
    protected $category_id = null;
    protected $alert_threshold = 10;
    protected $selling_price = 10;
    protected $total_stock_count = 0;
    protected $is_active = true;
    protected $deleted = false;
    protected $is_audit = true;
    protected $data_types = [
        'product_id' => 'int',
        'category_id' => 'int',
        'alert_threshold' => 'int',
        'is_active' => 'bool',
        'deleted' => 'bool',
        'selling_price' => 'float',
        'total_stock_count' => 'float'
    ];

    public function __construct() {
        $this->setRelation("category", "category_id", "CategoryModel", "category_id", ["name"]);
        parent::__construct();
    }

//    protected function beforeInsert($params) {
//        if (isset($params['product_key']) && empty($params['product_key'])) {
//            $id_col = self::ID_COL;
//            $params['product_key'] = $this->generateProductID($params[$id_col]);
//        }
//        return parent::beforeInsert($params);
//    }
    
    protected function afterInsert($params) {
        if (isset($params->product_key) && empty($params->product_key)) {
            $id_col = self::ID_COL;
            $params->product_key = $this->generateProductID($params->$id_col);
            $this->save($params);
        }
        return parent::afterInsert($params);
    }

    protected function validate($params) {
        $this->setRule("name", "required");
        $this->setRule("category_id", "required");
        $this->setRule("alert_threshold", "required|integer");
        $this->setRule("selling_price", "required|integer|greater_than[0]");
        return parent::validate($params);
    }
    
    protected function validateBeforeUpdate($params) {
        $filters[self::ID_COL . " !="] = $params[self::ID_COL];
        $filters[self::UNIQUE_COL] = $params[self::UNIQUE_COL];
        
        if ( $this->findFirst($filters) ){
            $this->addModelError("product_key", "The Product Key already exist.");
            return false;
        }
        return parent::validateBeforeUpdate($params);
    }
    
    protected function validateBeforeInsert($params) {
        if ( isset($params['product_key']) && !empty($params['product_key']) ){
            $this->setRule("product_key", "is_unique[".(self::TABLE).".".(self::UNIQUE_COL)."]", "Product Key");
        }
        return parent::validateBeforeInsert($params);
    }

    private function generateProductID($insertId) {
        return "PRD-" . str_pad($insertId, 6, "0", STR_PAD_LEFT);
    }

}

<?php

class User extends BaseModel {

    const TABLE = 'user';
    const ID_COL = 'user_id';
    
    protected $user_id = null;
    protected $user_hash = "";
    protected $name = "";
    protected $password = "";
    
    protected $data_types = [
        'user_id' => 'int'
    ];

    public function __construct() {
        parent::__construct();
    }

}

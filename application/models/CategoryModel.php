<?php

class CategoryModel extends BaseModel {

    const TABLE = 'category';
    const ID_COL = 'category_id';
    const UNIQUE_COL = 'name';

    protected $category_id = null;
    protected $name = null;
    protected $is_active = true;
    protected $deleted = false;
    
    protected $is_audit = true;
    protected $data_types = [
        'category_id' => 'int',
        'is_active' => 'bool',
        'deleted' => 'bool'
    ];

    public function __construct() {
        parent::__construct();
    }

    protected function validate($params) {
        $this->setRule("name", "required|is_unique[".(self::TABLE).".".(self::UNIQUE_COL)."]", "Category Name");
        return parent::validate($params);
        
    }

}

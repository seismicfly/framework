<script type="text/ng-template" id="delete-confirm.html">
    <div class="modal-header">
    <h3 class="modal-title" id="modal-title-{{vm.id}}">Delete {{vm.title}}</h3>
    </div>
    <div class="modal-body" id="modal-body-{{vm.id}}">
    Are you sure you want to delete <strong>{{vm.name}}</strong>?
    </div>
    <div class="modal-footer">
        <button class="btn btn-default" type="button" ng-click="vm.no()">No</button>
        <button class="btn btn-danger" type="button" ng-click="vm.yes()">Yes</button>
    </div>
</script>
</div></div>
</body>
</html>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo (!empty($this->page_title)) ? SITE_TITLE . ' - ' . $this->page_title : SITE_TITLE; ?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/css/bootstrap3/bootstrap-switch.min.css">
        <link rel="stylesheet" href="<?php echo Utility::getResourcesBasePath(); ?>common.css">
        <link rel="stylesheet" href="<?php echo Utility::getCssBasePath(); ?>pace.css">
        <link rel="stylesheet" href="<?php echo Utility::getCssBasePath(); ?>angular-moment-picker.min.css">
        <link rel="stylesheet" href="//npmcdn.com/angular-toastr/dist/angular-toastr.css" />
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/angular-ui-select/0.19.4/select.min.css" />
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />

        <?php echo Utility::getBaseVars($this); ?>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.2.16/angular-resource.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/2.1.4/ui-bootstrap.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-animate.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.8/angular-route.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.2.8/angular-ui-router.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/angular-touch/1.5.8/angular-touch.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/js/bootstrap-switch.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/angular-bootstrap-switch/0.5.1/angular-bootstrap-switch.min.js"></script>
        <script src="//npmcdn.com/angular-toastr/dist/angular-toastr.tpls.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/angular-sanitize/1.5.8/angular-sanitize.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-select/0.19.4/select.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/angular-smart-table/2.1.8/smart-table.min.js"></script>
        <script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-2.3.1.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment-with-locales.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/angular-moment/1.0.1/angular-moment.min.js"></script>
        <!--<script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.4.0.js"></script>-->
        <script src="<?php echo Utility::getResourcesBasePath(); ?>app.js"></script>
        <script src="<?php echo Utility::getJsBasePath(); ?>angular-moment-picker.min.js"></script>
        <script src="<?php echo Utility::getResourcesBasePath(); ?>common.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>
        <?php
        echo (Utility::getControllerCssPath($this)) ? "<link rel='stylesheet' href='" . Utility::getControllerCssPath($this) . "'>" : '';
        echo (Utility::getControllerJsPath($this)) ? "<script src='" . Utility::getControllerJsPath($this) . "'></script>" : '';
//        
//        
//        echo  Utility::getControllerJsPath($this);
//        exit;
        ?>

        <!--
        IE8 support, see AngularJS Internet Explorer Compatibility http://docs.angularjs.org/guide/ie
        For Firefox 3.6, you will also need to include jQuery and ECMAScript 5 shim
        -->
        <!--[if lt IE 9]>
          <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.js"></script>
          <script src="http://cdnjs.cloudflare.com/ajax/libs/es5-shim/2.2.0/es5-shim.js"></script>
          <script>
            document.createElement('ui-select');
            document.createElement('ui-select-match');
            document.createElement('ui-select-choices');
          </script>
        <![endif]-->

    <body>

        <?php
        echo Utility::getModuleInjections($this);
//        echo "<pre>".print_r($this, true)."</pre>";
        //menu
//    Utility::getControllerMenuContent($this);
        ?>
        <div id="container" class="container">
            <div ng-app="app" ng-controller="<?php echo $this->js_controller; ?> as vm" ng-cloak>
                <?php
//            echo '<pre>' . print_r($this, true) . '</pre>';
                ?>

                <!--https://indrimuska.github.io/angular-moment-picker/-->
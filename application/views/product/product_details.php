<?php
//echo "<pre>" . print_r($_REQUEST, true) . "</pre>";
//echo "<pre>" . print_r($var->id, true) . "</pre>";
?>
<form class="form-horizontal" name="dataForm" method="post" novalidate>
    <div class="panel panel-default">
        <div class="panel-heading"><h3 class="panel-title">{{vm.product.product_id? 'Update': 'Add'}} Product</h3></div>
        <div class="panel-body">
            <div class="col-md-10">
                <div class="form-group">
                    <label class="control-label col-sm-3" for="product_key">Product Key:</label>
                    <div class="col-sm-9">
                        <input type="text" ng-model="vm.product.product_key" class="form-control" id="product_key" name="product_key" placeholder="Assigned automatically if not given">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="name">Product Name:</label>
                    <div class="col-sm-9">
                        <input type="text" ng-model="vm.product.name" class="form-control" id="name" name="name" placeholder="Product Name">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="description">Product Description:</label>
                    <div class="col-sm-9">
                        <input type="text" ng-model="vm.product.description" class="form-control" id="description" name="description" placeholder="Product Description">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="category_id">Product Category:</label>
                    <div class="col-sm-9">
                        <ui-select ng-model="vm.product.category_id" id="category_id" name="category_id">
                            <ui-select-match placeholder="Type to search">{{ (vm.categories | filter : { category_id : vm.product.category_id } : true)[0].name}}</ui-select-match>
                            <ui-select-choices repeat="category.category_id as category in (vm.categories | filter: {name: $select.search})">
                                <span ng-bind-html="category.name | highlight: $select.search"></span>
                            </ui-select-choices>
                            <!--                        <ui-select-no-choice>
                                                        Dang!  We couldn't find any choices...
                                                    </ui-select-no-choice>-->
                        </ui-select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="alert_threshold">Alert Threshold:</label>
                    <div class="col-sm-2">
                        <input type="number" ng-model="vm.product.alert_threshold" class="form-control" id="alert_threshold" name="alert_threshold">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="selling_price">Selling Price:</label>
                    <div class="col-sm-2">
                        <input type="number" ng-model="vm.product.selling_price" class="form-control" id="selling_price" name="selling_price">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="is_active">Active:</label>
                    <div class="col-sm-2">
                        <!--<input type="number" ng-model="vm.product.is_active" class="form-control" id="is_active" name="is_active">-->
                        <input bs-switch type="checkbox" ng-model="vm.product.is_active" switch-on-text="Yes" switch-off-text="No" class="form-control" id="is_active" name="is_active"/>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title">Stocks in Hand</h3></div>
                    <div class="panel-body">
                        <div class="stock-in-hand">{{ vm.product.total_stock_count }}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer clearfix">
            <button type="submit" class="btn btn-primary pull-right" ng-click="vm.save();"><span>{{vm.product.product_id? 'Update': 'Save'}}</span></button>
        </div>
    </div>
    <div ng-show="vm.product.product_id" class="panel panel-default">
        <div class="panel-heading">
            <div class="clearfix">
                <h3 class="panel-title pull-left">Stock list for {{vm.product.name}}</h3>
                <a href="<?php echo base_url(); ?>stock/0?pid={{ vm.product.product_id}}" class="btn btn-success btn-sm pull-right" title="Add Stock Batch"><span class="glyphicon glyphicon-plus"></span> Add Stock Batch</a>
            </div>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-hover" st-pipe="vm.fetchStockData" st-table="vm.stockList" st-safe-src="vm.stockList">
                <thead>
                    <tr>
                        <th st-sort="name">Batch Name</th>
                        <th st-sort="purchase_quantity">Price</th>
                        <th st-sort="purchase_quantity">Purchased Quantity</th>
                        <th st-sort="purchase_quantity">Available Quantity</th>
                        <th st-sort="purchase_quantity">Purchased On</th>
                        <th></th>
                    </tr>
                    <tr>
                        <th><input st-search="iname" class="form-control"/></th>
                        <th><input st-search="fpurchase_price" class="form-control"/></th>
                        <th><input st-search="fpurchase_quantity" class="form-control"/></th>
                        <th><input st-search="favailable_quantity" class="form-control"/></th>
                        <th><input st-search="fpurchase_date" placeholder="YYYY-MM-DD" class="form-control"/></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody ng-show="!vm.isTableLoading">
                    <tr ng-repeat="row in vm.stockList">
                        <td>{{row.name}}</td>
                        <td>{{row.purchase_price}}</td>
                        <td>{{row.purchase_quantity}}</td>
                        <td>{{row.available_quantity}}</td>
                        <td>{{row.purchase_date| amParse:'YYYY-MM-DD' | amDateFormat:'LL'}}</td>
                        <!--<td><span am-time-ago="row.purchase_date | amParse:'YYYY.MM.DD HH:mm:ss'"></span></td>-->
                        <td nowrap>
                            <a href="<?php echo base_url(); ?>stock/{{row.stock_id}}?pid={{vm.product.product_id}}" class="btn btn-default btn-xs" title="Edit Details"><i class="fa fa-pencil"></i></a>
                            <button ng-click="vm.deleteStockItem(row)" class="btn btn-default btn-xs" title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
                        </td>
                    </tr>
                </tbody>
                <tbody ng-show="vm.isTableLoading">
                    <tr>
                        <td colspan="6" class="text-center">
                            <i class="fa fa-circle-o-notch fa-spin fa-fw"></i> Loading...
                        </td>
                    </tr>
                </tbody>
                <tbody ng-hide="vm.isTableLoading || vm.stockList.length">
                    <tr>
                        <td colspan="6" class="text-center">
                            No Stocks Found
                        </td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr><td class="text-center" st-pagination="" st-items-by-page="10" colspan="6"></td></tr>
                </tfoot>
            </table>
        </div>
    </div>
</form>

<div class="panel panel-default">
    <div class="panel-heading">
        <div class="clearfix">
            <h3 class="panel-title">Product Summary</h3>
            <a href="<?php echo base_url(); ?>product/0" class="btn btn-success btn-sm pull-right" title="Add Product"><span class="glyphicon glyphicon-plus"></span> Add Product</a>
        </div>
    </div>
    <div class="panel-body">
        <table class="table table-striped table-hover" st-pipe="vm.fetchData" st-table="vm.data" st-safe-src="vm.data">
            <thead>
                <tr>
                    <th st-sort="product_key">ID</th>
                    <th st-sort="name">Name</th>
                    <th st-sort="description">Desc.</th>
                    <th st-sort="category_id">Category</th>
                    <th st-sort="selling_price" class="text-right">Selling Price</th>
                    <th></th>
                </tr>
                <tr>
                    <th><input st-search="iproduct_key" class="form-control"/></th>
                    <th><input st-search="sname" class="form-control"/></th>
                    <th><input st-search="ldescription" class="form-control"/></th>
                    <th>
                        <select st-search="icategory_id" class="form-control">
                            <option value="">All</option>
                            <option ng-repeat="category in vm.categories" value="{{category.category_id}}">{{category.name}}</option>
                        </select>
                    </th>
                    <th><input st-search="fselling_price" class="form-control"></th>
                    <th></th>
                </tr>
            </thead>
            <tbody ng-show="!vm.isTableLoading">
                <tr ng-repeat="row in vm.data">

                    <td><a href="<?php echo base_url(); ?>product/{{row.product_id}}">{{row.product_key}}</a></td>
                    <td>{{row.name}}</td>
                    <td>{{row.description}}</td>
                    <td>{{row.category.name}}</td>
                    <td class="text-right">{{row.selling_price}}</td>
                    <td>
                        <a href="<?php echo base_url(); ?>product/{{row.product_id}}" class="btn btn-default btn-xs" title="Edit Details"><i class="fa fa-pencil"></i></a>
                        <button ng-click="vm.deleteProduct(row)" class="btn btn-default btn-xs" title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
                    </td>
                </tr>
            </tbody>
            <tbody ng-show="vm.isTableLoading">
                <tr>
                    <td colspan="7" class="text-center">
                        <i class="fa fa-circle-o-notch fa-spin fa-fw"></i> Loading...
                    </td>
                </tr>
            </tbody>
            <tbody ng-hide="vm.isTableLoading || vm.data.length">
                <tr>
                    <td colspan="7" class="text-center">
                        No Records Found
                    </td>
                </tr>
            </tbody>
            <tfoot>
                <tr><td class="text-center" st-pagination="" st-items-by-page="25" colspan="7"></td></tr>
            </tfoot>
        </table>
    </div>
    <div class="panel-footer clearfix"></div>
</div>


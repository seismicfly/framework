<form class="form-horizontal" name="dataForm" method="post">
    <div class="panel panel-default">
        <div class="panel-heading"><h3 class="panel-title">Settings</h3></div>
        <div class="panel-body">

            <div class="form-group">
                <label class="control-label col-sm-2" for="name">Stock Selection:</label>
                <div class="col-sm-9">
                    <input type="text" ng-model="vm.settings.stock_selection" class="form-control" id="stock_selection" name="stock_selection" placeholder="Stock Batch Name">
                </div>
            </div>
            
            <!--
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="purchase_price">Purchased Price:</label>
                            <div class="col-sm-9" ng-class="{'form-control-static': vm.stock.stock_id}">
                                <input type="number" ng-if="!vm.stock.stock_id" ng-model="vm.stock.purchase_price" class="form-control" id="purchase_price" name="purchase_price" placeholder="Purchased Price">
                                <span ng-if="vm.stock.stock_id">{{vm.stock.purchase_price}}</span>
                            </div>
                        </div>
            
                        <div class="form-group" ng-if="!vm.stock.stock_id">
                            <label class="control-label col-sm-2" for="purchase_quantity">Purchased Quantity:</label>
                            <div class="col-sm-9">
                                <input type="number" ng-model="vm.stock.purchase_quantity" class="form-control" id="purchase_price" name="purchase_quantity" placeholder="Purchased Quantity">
                            </div>
                        </div>
            
                        <div class="form-group" ng-if="vm.stock.stock_id">
                            <label class="control-label col-sm-2" for="purchase_quantity">Quantity:</label>
                            <div class="col-sm-9 form-control-static">
                                <strong>{{vm.stock.available_quantity}}</strong> / {{vm.stock.purchase_quantity}} 
                                <span class="text-muted">(Available / Purchased Quantity)</span>
                            </div>
                        </div>
            
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="purchase_date">Purchased On:</label>
                            <div class="col-sm-9">
                                <div class="input-group"
                                     moment-picker="vm.stock.purchase_date">
                                    <input class="form-control"
                                           placeholder="Select a date"
                                           ng-model="vm.stock.purchase_date">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>-->

        </div>
        <div class="panel-footer clearfix">
            <!--<button type="submit" class="btn btn-primary pull-right" ng-click="vm.save();"><span>{{vm.stock.stock_id? 'Update': 'Save'}}</span></button>-->
        </div>
    </div>
</form>

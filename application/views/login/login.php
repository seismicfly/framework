<h1>Test</h1>
<form name="dataForm">
    <div class="form-group">
        <label for="usr">Name:</label>
        <input type="text" class="form-control" id="name" name="name" ng-value="" ng-model="vm.user.name">
        
    </div>
    <div class="form-group">
        <label for="pwd">Password:</label>
        <input type="password" class="form-control" id="password" ng-value="" name="password" ng-model="vm.user.password">
    </div>

    <button type="submit" ng-click="vm.doLogin()" class="btn btn-primary">Login</button>
</form>
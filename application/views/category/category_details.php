<form class="form-horizontal" name="dataForm" method="post">
    <div class="panel panel-default">
        <div class="panel-heading"><h3 class="panel-title">{{vm.category.category_id? 'Update': 'Add'}} Category</h3></div>
        <div class="panel-body">

            <div class="form-group">
                <label class="control-label col-sm-2" for="name">Category Name:</label>
                <div class="col-sm-9">
                    <input type="text" ng-model="vm.category.name" class="form-control" id="name" name="name" placeholder="Category Name">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-2" for="is_active">Active:</label>
                <div class="col-sm-2">
                    <input bs-switch type="checkbox" ng-model="vm.category.is_active" switch-on-text="Yes" switch-off-text="No" class="form-control" id="is_active" name="is_active"/>
                </div>
            </div>

        </div>
        <div class="panel-footer clearfix">
            <button type="submit" class="btn btn-primary pull-right" ng-click="vm.save();"><span>{{vm.category.category_id? 'Update': 'Save'}}</span></button>
        </div>
    </div>
</form>

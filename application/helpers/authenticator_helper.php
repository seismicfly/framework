<?php

class Authenticator {

    const USER_TYPE = 'admin';

    private static $user_info = [
        'user_id' => 5
    ];
    
    public static function setUser($param) {
        self::$user_info = $param;
    }

    public static function authCheck($controller = '') {
        if ($controller != 'rest') {
            if (self::isAuthenticatedUser()) {
                if (self::isRedirect($controller)) {
                    redirect(base_url('home'));
                }
            } else {
                if (!self::isRedirect($controller)) {
                    redirect(base_url(LOGIN_CONTROLLER));
                }
            }
        }
    }

    private static function isRedirect($param) {
        return (LOGIN_CONTROLLER == $param);
    }

    public static function isAuthenticatedUser() {
        $CI = get_instance();
        $CI->load->model('UserSession');
        $user_hash = self::getAuthCookie();
        if ($user_hash) {
            $fltr['user_hash'] = $user_hash;
            $user_session = $CI->UserSession->findFirst($fltr);
            if ($user_session) {
                if ($user_session->remember_me) {
                    return true;
                } elseif ($user_session->end_time > $CI->UserSession->dateTime()) {
                    return true;
                }
            }
        }
        self::deleteAuthCookie();
        return false;
    }

    public static function setAuthCookie($user_hash) {
        set_cookie(USER_SESSION, $user_hash, (time() + (48 * 60 * USER_SESSION_DURATION)));
    }

    public static function getAuthCookie() {
        return get_cookie(USER_SESSION);
    }

    public static function deleteAuthCookie() {
        delete_cookie(USER_SESSION);
    }

    public static function getUserId() {
        return self::$user_info['user_id'];
    }

}

<?php

class Utility {

    public static function getResourcesBasePath() {
        return base_url() . "resources/";
    }

    public static function getCssBasePath() {
        return base_url() . "resources/css/";
    }

    public static function getJsBasePath() {
        return base_url() . "resources/js/";
    }

    public static function getControllerJsPath($currentEntity) {
        $file_path = base_url() . "resources/{$currentEntity->controller}/{$currentEntity->view_name}.js";
        if (read_file($file_path)) {
            return $file_path;
        }
        return false;
    }

    public static function getControllerCssPath($currentEntity) {
        $file_path = base_url() . "resources/{$currentEntity->controller}/{$currentEntity->view_name}.css";
        if (read_file($file_path)) {
            return $file_path;
        }
        return false;
    }

    public static function getControllerMenuContent($currentEntity) {
        $file_path = APPPATH . 'views/' . $currentEntity->controller . '/menu.html';
        if (is_file($file_path)) {
            echo read_file($file_path);
        } else {
            echo DEBUG_MODE ? '<!--No menu.html found-->' : '';
        }
    }

    // This will add view_var as js variables to use in AJS
    public static function getModuleInjections($currentEntity) {
        $html = "";
//        echo "<pre>".print_r($currentEntity->view_vars, true)."</pre>";
        if (!empty($currentEntity->view_vars)) {
            $html .= "<script type='text/javascript'>";
            $html .= "(function() {";
            $html .= "app.constant('INJECTIONS', {";
            foreach ($currentEntity->view_vars as $key => $value) {
                if (is_object($value) ){
                    $encoded_value = json_encode($value);
                    $html .= "'{$key}' : {$encoded_value},";
                } else {
                    $html .= "'{$key}' : {$value},";
                }
            }
            $html .= "});";
            $html .= "}());";
            $html .= "</script>";
        }
        return $html;
    }
    
    public static function getBaseVars($currentEntity) {
        $html = "";
        if (!empty($currentEntity->base_vars)) {
            $html .= "<script type='text/javascript'>";
            foreach ($currentEntity->base_vars as $key => $value) {
                $html .= "var {$key} = '{$value}';";
            }
            $html .= "</script>";
        }
        return $html;
    }
    
    // Debug Help
    public static function debugResult($param){
        echo "<pre>".print_r($param, true)."</pre>";
        exit(0);
    }

}
